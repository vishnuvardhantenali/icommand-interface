﻿using System;
using System.Windows.Input;

namespace ICommandInterface
{
    public class Model :ICommand
    {
        public int SNo { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public ViewModel vm { get; set; }

        public Model(ViewModel vm)
        {
            this.vm = vm;
        }
        public Model(int sno, string fname, string lname)
        {
            this.SNo = sno;
            this.FName = fname;
            this.LName= lname;
        }        
        
        public event EventHandler CanExecuteChanged;
        
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            vm.OnExecute();
        }
    }
}
