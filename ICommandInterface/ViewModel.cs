﻿using System.Windows;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace ICommandInterface
{
    public class ViewModel: INotifyPropertyChanged
    {
        private string fname;
        private string lname;

        public int count { get; set; } = 1;
        public Model iCommandModel { get; set; }

        private ObservableCollection<Model> _models = new ObservableCollection<Model>();

        public ViewModel()
        {            
            PersonModel.Add(new Model(count++, "Vishnu", "Vardhan"));
            iCommandModel = new Model(this);
        }

        public ObservableCollection<Model> PersonModel
        {
            get { return _models; }
            set { _models = value; }
        }

        public string LName
        {
            get { return lname; }
            set
            {
                lname = value;
                PropChange("LName");
            }
        }

        public string FName
        {
            get { return fname; }
            set
            {
                fname = value;
                PropChange("FName");
            }
        }        

        public event PropertyChangedEventHandler PropertyChanged;
        public void PropChange(string value)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(value));
        }
        public void OnExecute()
        {
            if (string.IsNullOrEmpty(fname) || string.IsNullOrEmpty(lname)) return;
            PersonModel.Add(new Model(count++, fname, lname));            
            MessageBox.Show("Successfully Added to the listitems");
        }
    }    
}
